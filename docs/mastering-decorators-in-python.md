# Python 데코레이터 마스터하기: 고급 기능에 대한 종합 가이드

## 기본 사항 이해하기
고급 데코레이터 기술을 살펴보기 전에 기본 사항을 살펴보자. Python에서 데코레이터는 다른 함수를 인수로 받아 그 동작을 확장하거나 수정하는 함수이다. 데코레이터는 데코레이션할 함수 또는 메서드 위에 `@` 기호 뒤에 데코레이터 함수 이름을 붙여 표시한다.

```python
@decorator
def my_function():
    # function implementation
```

## 고급 데코레이터 기술

### 1. 매개변수화된 데코레이터
데코레이터를 매개변수화하여 더욱 다양하게 활용할 수 있다. 이를 통해 데코레이터는 인수를 받아 동작을 동적으로 수정할 수 있다. 다음 예를 생각해 보자.

```python
def repeat(n):
    def decorator(func):
        def wrapper(*args, **kwargs):
            for _ in range(n):
                func(*args, **kwargs)
        return wrapper
    return decorator

@repeat(3)
def greet(name):
    print(f"Hello, {name}!")

greet("Alice")
```

Output:

```
Hello, Alice!
Hello, Alice!
Hello, Alice!
```

여기서 `repeat` 데코레이터는 인자 `n`을 받아 데코레이션된 함수를 n번 실행하도록 수정한다.

### 2. 데코레이터 체인 연결하기
데코레이터를 서로 겹쳐서 함수의 체인을 만들 수 있다. 이는 하나의 함수에 여러 데코레이터를 적용하여 이루어진다. 체인의 각 데코레이터는 가장 안쪽에서 가장 바깥쪽으로 적용된다.

```python
@decorator1
@decorator2
@decorator3
def my_function():
    # function implementation
```

데코레이터의 순서는 데코레이터가 적용되는 순서를 결정하므로 매우 중요하다.

### 3. 클래스 데코레이터
데코레이터는 일반적으로 함수에 적용되지만 클래스에도 사용할 수 있다. 클래스 데코레이터는 클래스를 인수로 받아 클래스의 동작을 수정하거나 확장할 수 있다.

```python
def add_method(cls):
    def new_method(self):
        print("New method added to the class!")
    cls.new_method = new_method
    return cls

@add_method
class MyClass:
    def existing_method(self):
        print("Existing method in the class.")

obj = MyClass()
obj.existing_method()
obj.new_method()
```

Output:

```
Existing method in the class.
New method added to the class!
```

이 예에서 `add_method` 데코레이터는 클래스에 새 메서드를 추가한다.

### 4. 데코레이터 메모화
메모화는 비용이 많이 드는 함수 호출의 결과를 캐시해 두었다가 같은 입력이 다시 발생할 때 재사용하는 기술이다. 데코레이터는 메모화를 구현하는 우아한 방법을 제공한다.

```python
ef memoize(func):
    cache = {}

    def wrapper(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]

    return wrapper

@memoize
def fibonacci(n):
    if n <= 1:
        return n
    return fibonacci(n-1) + fibonacci(n-2)
```

`memorize` 데코레이터는 `fibonacci` 함수의 결과를 캐시하여 동일한 인수를 반복적으로 호출할 때 성능을 향상시킨다.

## 마치며
Python의 데코레이터는 함수와 메서드의 동작을 수정하거나 확장할 수 있는 유연하고 표현력 있는 방법을 제공한다. 이 종합 가이드에서 살펴본 바와 같이 데코레이터의 기능은 단순한 사용 사례 이상으로 확장할 수 있다. 매개변수화된 데코레이터, 체인 데코레이터, 클래스 데코레이터, 메모화 등은 데코레이터가 Python 프로그래밍에 제공하는 고급 기능의 몇 가지 예에 불과하다.

데코레이터를 마스터하면 프로그래머는 코드의 가독성, 유지보수성 및 전반적인 디자인을 크게 향상시킬 수 있다. 기능 추가, 액세스 제어, 성능 최적화 등 데코레이터는 Python 프로그래머의 무기고에서 매우 유용한 도구이다.
